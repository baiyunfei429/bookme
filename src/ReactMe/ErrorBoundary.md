---
nav:
  title: 【 React 】
  path: /components
---

## ErrorBoundary

React 16 的错误边界

### 简介

> A JavaScript error in a part of the UI shouldn’t break the whole app. To solve this problem for React users, React 16 introduces a new concept of an “error boundary”.  
> 部分 UI 的 JavaScript 错误不应该导致整个应用崩溃，为了解决这个问题，React 16 引入了一个新的概念 —— 错误边界。

> Error boundaries are React components that catch JavaScript errors anywhere in their child component tree, log those errors, and display a fallback UI instead of the component tree that crashed. Error boundaries catch errors during rendering, in lifecycle methods, and in constructors of the whole tree below them.  
> 错误边界是一种 React 组件，这种组件可以捕获发生在其子组件树任何位置的 JavaScript 错误，并打印这些错误，同时展示降级 UI，而并不会渲染那些发生崩溃的子组件树。错误边界在渲染期间、生命周期方法和整个组件树的构造函数中捕获错误。

注意：有些情况是 Error boundaries 捕获不到的：

- Event handlers(事件处理)。解决方案： 使用 try/catch 进行捕获
- Asynchronous code (e.g. setTimeout or requestAnimationFrame callbacks)(异步代码)解决方案：使用 window.addEventListener 进行捕获
- Server side rendering(服务端渲染)
- Errors thrown in the error boundary itself (rather than its children)。(它自身抛出来的错误,并非它的子组件)。意味着：error boundaries only catch errors in the components below them in the tree. An error boundary can’t catch an error within itself.

如果一个 class 组件中定义了 static getDerivedStateFromError() 或 componentDidCatch() 这两个生命周期方法中的任意一个（或两个）时，那么它就变成一个错误边界。当抛出错误后，请使用 static getDerivedStateFromError() 渲染备用 UI ，使用 componentDidCatch() 打印错误信息。

### 应该放置在哪？

错误边界的粒度由你来决定，可以将其包装在最顶层的路由组件并为用户展示一个 “Something went wrong” 的错误信息，就像服务端框架经常处理崩溃一样。你也可以将单独的部件包装在错误边界以保护应用其他部分不崩溃。

### 基本用法

```javascript
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    logErrorToMyService(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}

<ErrorBoundary>
  <MyWidget />
</ErrorBoundary>;
```

React 15 included a very limited support for error boundaries under a different method name: unstable_handleError. This method no longer works, and you will need to change it to componentDidCatch in your code starting from the first 16 beta release.

### promise 的捕获

componentDidCatch is not getting called when there is an error in promise.so, what should we do?

```javascript
// 基本用法是：使用react的状态更新方法来throw error
setState(() => {
  throw new Error('error');
});

// 情况一：在class组件中可以使用this.setState()
{
  try {
    fetchData();
  } catch (err) {
    this.setState(() => {
      throw err;
    });
  }
}
// 情况二：在hooks中可以使用setState()
{
  const [, /* state */ setState] = useState();
  setState(() => {
    throw error;
  });
}
// 情况二可以再抽象一点，同时也更coding一点
function useAsyncError() {
  const [, setError] = useState();
  return useCallback(
    err =>
      setError(() => {
        throw err;
      }),
    [setError],
  );
}

const throwAsyncError = useAsyncError();
return (
  <button
    onClick={() => {
      throwAsyncError(new Error(''));
    }}
  />
);
```

但是这些都是局部处理方案，不够全局，不够优雅。  
下面说一种全局处理方案: 在 componentDidMount 生命周期中添加 window.addEventListener('unhandledrejection',()=>{})

```javascript
interface State {
  error: any; // Could be an exception thrown in synchronous code or could be a rejection reason from a Promise, we don't care
}

class ErrorBoundary extends Component<State> {
  private promiseRejectionHandler = (event: PromiseRejectionEvent) => {
    this.setState({
      error: event.reason
    });
  }

  public state: State = {
    error: null
  };

  public static getDerivedStateFromError(error: Error): State {
    // Update state so the next render will show the fallback UI.
    return { error: error };
  }

  public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.error("Uncaught error:", error, errorInfo);
  }

  componentDidMount() {
    // Add an event listener to the window to catch unhandled promise rejections & stash the error in the state
    window.addEventListener('unhandledrejection', this.promiseRejectionHandler)
  }

  componentWillUnmount() {
    window.removeEventListener('unhandledrejection', this.promiseRejectionHandler);
  }

  public render() {
    if (this.state.error) {
      const error = this.state.error;

      let errorName;
      let errorMessage;

      if (error instanceof PageNotFoundError) {
        errorName = "...";
        errorMessage = "..."
      } else if (error instanceof NoRolesAssignedError) {
        errorName = "...";
        errorMessage = "..."
      } else {
        errorName = "Unexpected Application Error";
      }

      return <FriendlyError errorName={errorName} errorMessage={errorMessage} />
    } else {
      return this.props.children;
    }
  }
}
```

### 参考文章列表

---

react 官网： https://reactjs.org/docs/error-boundaries.html  
https://github.com/facebook/react/issues/11409  
https://github.com/facebook/react/issues/14981  
https://eddiewould.com/2021/28/28/handling-rejected-promises-error-boundary-react/
