---
nav:
  title: 【 React 】
  path: /components
---

## React

Demo:

```tsx
import React from 'react';
import { ReactMe } from 'bjg-blog';

export default () => <ReactMe title="First React" />;
```
