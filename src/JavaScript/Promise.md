---
nav:
  title: 【 JavaScript 】
  path: /components
---

## Promise

ECMAscript 6 原生提供了 Promise 对象。

Promise 对象代表了未来将要发生的事件，用来传递异步操作的消息。

Promise 对象有以下两个特点:
1、对象的状态不受外界影响。Promise 对象代表一个异步操作，有三种状态：

pending: 初始状态，不是成功或失败状态。
fulfilled: 意味着操作成功完成。
rejected: 意味着操作失败。
只有异步操作的结果，可以决定当前是哪一种状态，任何其他操作都无法改变这个状态。这也是 Promise 这个名字的由来，它的英语意思就是「承诺」，表示其他手段无法改变。

2、一旦状态改变，就不会再变，任何时候都可以得到这个结果。Promise 对象的状态改变，只有两种可能：从 Pending 变为 Resolved 和从 Pending 变为 Rejected。只要这两种情况发生，状态就凝固了，不会再变了，会一直保持这个结果。就算改变已经发生了，你再对 Promise 对象添加回调函数，也会立即得到这个结果。这与事件（Event）完全不同，事件的特点是，如果你错过了它，再去监听，是得不到结果的。

Promise 优缺点
有了 Promise 对象，就可以将异步操作以同步操作的流程表达出来，避免了层层嵌套的回调函数。此外，Promise 对象提供统一的接口，使得控制异步操作更加容易。

Promise 也有一些缺点。首先，无法取消 Promise，一旦新建它就会立即执行，无法中途取消。其次，如果不设置回调函数，Promise 内部抛出的错误，不会反应到外部。第三，当处于 Pending 状态时，无法得知目前进展到哪一个阶段（刚刚开始还是即将完成）。

### 手写一个简单的 promise(不支持异步回调)

```tsx
const RESOLVED = 'RESOLVED';
const REJECTED = 'REJECTED';
const PENDING = 'PENDING';
class Promise1 {
  constructor(fn) {
    this.status = PENDING;
    this.value = undefined;
    this.reason = undefined;
    let resolve = data => {
      if (this.status === PENDING) {
        this.value = data;
        this.status = RESOLVED;
      }
    };
    let reject = reason => {
      if (this.status === PENDING) {
        this.reason = reason;
        this.status = REJECTED;
      }
    };
    try {
      fn(resolve, reject);
    } catch (e) {
      reject(e);
      console.error(e);
    }
  }
  then(onFullfilled, onRejected) {
    if (this.status === RESOLVED) {
      onFullfilled(this.value);
    }
    if (this.status === REJECTED) {
      onRejected(this.reason);
    }
  }
}
/*
let pro = new Promise1((res, rej) => {
  // throw new Error('失败了-bai')
  // res('成功-yun');
  setTimeout(() => {
    rej('失败-fei');
  }, 1000)
});
pro.then(
  data => {
    console.log('success', data);
  },
  err => {
    console.log('failed', err);
  },
);
*/
```

### 手写一个简单的 promise(支持异步回调)

通过发布订阅模式实现

```tsx
const RESOLVED = 'RESOLVED';
const REJECTED = 'REJECTED';
const PENDING = 'PENDING';
class Promise2 {
  constructor(fn) {
    this.status = PENDING;
    this.value = undefined;
    this.reason = undefined;
    this.onResolvedCallbacks = []; // 用于存放成功的回调
    this.onRejectedCallbacks = []; // 用于存放失败的回调
    let resolve = data => {
      if (this.status === PENDING) {
        this.value = data;
        this.status = RESOLVED;
        this.onResolvedCallbacks.forEach(fn => fn());
      }
    };
    let reject = reason => {
      if (this.status === PENDING) {
        this.reason = reason;
        this.status = REJECTED;
        this.onRejectedCallbacks.forEach(fn => fn());
      }
    };
    try {
      fn(resolve, reject);
    } catch (e) {
      reject(e);
      console.error(e);
    }
  }
  then(onFullfilled, onRejected) {
    if (this.status === RESOLVED) {
      onFullfilled(this.value);
    }
    if (this.status === REJECTED) {
      onRejected(this.reason);
    }
    if (this.status === PENDING) {
      this.onResolvedCallbacks.push(() => {
        // something to do……
        onFullfilled(this.value);
      });
      this.onRejectedCallbacks.push(() => {
        // something to do……
        onRejected(this.reason);
      });
      console.log('等待状态……');
    }
  }
}
// let pro = new Promise2((res, rej) => {
//   // throw new Error('失败了-bai')
//   // res('成功-yun');
//   setTimeout(() => {
//     // rej('失败-fei');
//     res('成功-yun');
//   }, 1000);
// });
// pro.then(
//   data => {
//     console.log('success', data);
//   },
//   err => {
//     console.log('failed', err);
//   },
// );

// pro.then(
//   data => {
//     console.log('success2', data);
//   },
//   err => {
//     console.log('failed2', err);
//   },
// );
```

### 手写一个简单的 promise(支持 promises A+)

promises/A+： https://promisesaplus.com/

<!-- promise.then().then().then().catch() -->

```tsx
const RESOLVED = 'RESOLVED';
const REJECTED = 'REJECTED';
const PENDING = 'PENDING';
// 所有的promise都要遵循规范，包括bluebird/q/es6-promise
const resolvePromise = (promise2, x, resolve, reject) => {
  // 循环引用，错误的实现
  if (promise2 === x) {
    return reject(
      new TypeError('Chaining cycle detected for promise #<Promise>'),
    );
  }
  let called; // 别人的promise可能既成功又失败，所以需要一个“锁”，保证只能出现一种结果
  if ((typeof x === 'object' && x != null) || typeof x === 'function') {
    // 有可能是promise，继续判断
    try {
      // 之所以需要try catch，是因为别人定义的promise有坑是Object.defineProperty(x, 'then', {get() {throw new Error();} })
      let then = x.then;
      if (typeof then === 'function') {
        // 只能是promise
        // 此处为什么不用call.then()来执行呢？因为get() {if(++index === 2) throw new Error()},也就是上面定义then时不报错，但是再次使用call.then()时，会再次取值，就报错了，别人的promise不完全可信
        then.call(
          x,
          y => {
            // 根据promise的状态决定是成功还是失败
            if (called) return;
            called = true;
            // resolve(y)
            // 如果y还是promise呢，需要递归处理
            resolvePromise(promise2, y, resolve, reject);
          },
          e => {
            if (called) return;
            called = true;
            reject(e);
          },
        );
      } else {
        // {then: '3333'}
        resolve(x);
      }
    } catch (err) {
      // 防止失败了，再次进入成功
      if (called) return;
      called = true;
      reject(err); // 取值出错直接返回reject
      console.error(err);
    }
  } else {
    // 如果不是object,不是function,也就是普通值，直接resolve
    resolve(x);
  }
  // console.log(promise2, x, resolve, reject)
};
class Promise3 {
  constructor(fn) {
    this.status = PENDING;
    this.value = undefined;
    this.reason = undefined;
    this.onResolvedCallbacks = []; // 用于存放成功的回调
    this.onRejectedCallbacks = []; // 用于存放失败的回调
    let resolve = data => {
      if (this.status === PENDING) {
        this.value = data;
        this.status = RESOLVED;
        this.onResolvedCallbacks.forEach(fn => fn());
      }
    };
    let reject = reason => {
      if (this.status === PENDING) {
        this.reason = reason;
        this.status = REJECTED;
        this.onRejectedCallbacks.forEach(fn => fn());
      }
    };
    try {
      fn(resolve, reject);
    } catch (e) {
      console.error(e, 'err-bai');
      reject(e);
    }
  }
  then(onFullfilled, onRejected) {
    // 当onFullfilled没有时，那么为了实现.then().then().then(data=>console.log(data))，可以正确返回，那么默认onFullfilled = v => {return v}
    onFullfilled = typeof onFullfilled === 'function' ? onFullfilled : v => v;
    onRejected =
      typeof onRejected === 'function'
        ? onRejected
        : err => {
            throw err;
          };
    let promise2 = new Promise3((resolve, reject) => {
      // 实现链式调用
      if (this.status === RESOLVED) {
        setTimeout(() => {
          try {
            let x = onFullfilled(this.value);
            // x可能是Promise
            console.log('onFullfilled');
            console.log('onFullfilled-promise2', promise2);
            resolvePromise(promise2, x, resolve, reject);
          } catch (err) {
            reject(err);
          }
        }, 0);
      }
      if (this.status === REJECTED) {
        setTimeout(() => {
          try {
            let x = onRejected(this.reason);
            console.log('onRejected');
            console.log('onRejected-promise2', promise2);
            resolvePromise(promise2, x, resolve, reject);
          } catch (err) {
            reject(err);
          }
        }, 0);
      }
      if (this.status === PENDING) {
        this.onResolvedCallbacks.push(() => {
          setTimeout(() => {
            try {
              // something to do……
              let x = onFullfilled(this.value);
              resolvePromise(promise2, x, resolve, reject);
            } catch (err) {
              reject(err);
            }
          }, 0);
        });
        this.onRejectedCallbacks.push(() => {
          setTimeout(() => {
            try {
              // something to do……
              let x = onRejected(this.reason);
              resolvePromise(promise2, x, resolve, reject);
            } catch (err) {
              reject(err);
            }
          }, 0);
        });
        console.log('等待状态……');
      }
    });
    return promise2;
  }
  // 支持.catch方法
  catch(errorCallback) {
    return this.then(null, errorCallback);
  }
}
// let p1 = new Promise3((resolve,reject) => {
//   resolve(100)
// })
// let p2 = p1.then(data => {
//   throw new Error()
// }, err => {
//   return '失败'
// })
// p2.then(data => {
//   console.log('data*****', data)
// }, err => {
// })

// let p1 = new Promise3((resolve,reject) => {
//   resolve(100)
// })

// let p2 = p1.then(data => {
//   console.log('11*****', data)
//   return p2
// }, err => {
//   return '失败'
// })

// p2.then(data => {
//   console.log('data*****', data)
// }, err => {
//   console.log('error*****', err)
// })

function readFile() {
  return new Promise3((res, rej) => {
    setTimeout(() => {
      // rej('失败-fei');
      // res('成功2-fei');
      // res(1000);
      rej(3000);
      // return 23
    }, 1000);
  });
}
// readFile()
//   .then(data => {
//     console.log(data, 'data100');
//     // return 100;
//     // throw new Error()
//     return new Promise3((res,rej) => {
//       // res('成功-yun222');
//       rej('失败-fei');
//     })
//   }, err=> {
//     console.log('error1', err)
//     return '200'
//   })
//   .then(data => {
//     console.log('success2', data);
//     return data;
//   },err=>{
//     console.log('error2', err)
//     return 1;
//   })
//   .then(data => {
//     console.log('success3', data);
//     // return data;
//   });

// readFile()
// .then(data => {
//   console.log(data, 'data--');
//   return new Promise3((res,rej) => {
//     setTimeout(() => {
//       res(new Promise3((res, rej) => {
//         setTimeout(() => {
//           res(666)
//         }, 100)
//       }));
//     }, 1000)
//   })
// }, err=> {
//   console.log('error1', err)
//   return '200'
// })
// .then(data => {
//   console.log('success222', data);
//   return data;
// },err=>{
//   console.log('error222', err)
//   return 1;
// })

// readFile().then(data => {
//   return data
//   console.log(data, 'data===1')
// }).then(data => {
//   return data
//   console.log(data, 'data===2')
// }).then(data => {
//   console.log(data, 'data===3')
// })
// readFile().then().then().then(data => {
//   console.log(data, 'data===3')
// },err => {
//   console.log(err, 'err==3')
// })

// readFile().then(null, data => {
//   throw data
//   console.log(data, 'data===1')
// }).then(null, data => {
//   throw data
//   console.log(data, 'data===2')
// }).then(res=> {
//   console.log(res, 'res===')
// }, err => {
//   console.log(err, 'err===3')
// })
// readFile()
//   .then()
//   .then()
//   .then(
//     data => {
//       console.log(data, 'data===4');
//     },
//     err => {
//       console.log(err, 'err==4');
//     },
//   );
```

### 是否符合 promise A+规范的测试

```tsx
// promise的延迟对象，可以避免引用promise时，产生的回调嵌套写法
Promise.defer = Promise.deferred = function() {
  let dfd = {};
  dfd.promise = new Promise((resolve, reject) => {
    dfd.resolve = resolve;
    dfd.reject = reject;
  });
  return dfd;
};
// npm install promises-aplus-tests -g
// promises-aplus-tests [your-promise-file.js]
```

### promise 的两个静态方法 Promise.resolve()和 Promise.reject()和 Promise.prototype.finally

区别是：resolve 会等待 promise 执行完毕，reject 不会等待

```tsx
const RESOLVED = 'RESOLVED';
const REJECTED = 'REJECTED';
const PENDING = 'PENDING';
// 所有的promise都要遵循规范，包括bluebird/q/es6-promise
const resolvePromise = (promise2, x, resolve, reject) => {
  // 循环引用，错误的实现
  if (promise2 === x) {
    return reject(
      new TypeError('Chaining cycle detected for promise #<Promise>'),
    );
  }
  let called; // 别人的promise可能既成功又失败，所以需要一个“锁”，保证只能出现一种结果
  if ((typeof x === 'object' && x != null) || typeof x === 'function') {
    // 有可能是promise，继续判断
    try {
      // 之所以需要try catch，是因为别人定义的promise有坑是Object.defineProperty(x, 'then', {get() {throw new Error();} })
      let then = x.then;
      if (typeof then === 'function') {
        // 只能是promise
        // 此处为什么不用call.then()来执行呢？因为get() {if(++index === 2) throw new Error()},也就是上面定义then时不报错，但是再次使用call.then()时，会再次取值，就报错了，别人的promise不完全可信
        then.call(
          x,
          y => {
            // 根据promise的状态决定是成功还是失败
            if (called) return;
            called = true;
            // resolve(y)
            // 如果y还是promise呢，需要递归处理
            resolvePromise(promise2, y, resolve, reject);
          },
          e => {
            if (called) return;
            called = true;
            reject(e);
          },
        );
      } else {
        // {then: '3333'}
        resolve(x);
      }
    } catch (err) {
      // 防止失败了，再次进入成功
      if (called) return;
      called = true;
      reject(err); // 取值出错直接返回reject
      console.error(err);
    }
  } else {
    // 如果不是object,不是function,也就是普通值，直接resolve
    resolve(x);
  }
  // console.log(promise2, x, resolve, reject)
};
class Promise3 {
  constructor(fn) {
    this.status = PENDING;
    this.value = undefined;
    this.reason = undefined;
    this.onResolvedCallbacks = []; // 用于存放成功的回调
    this.onRejectedCallbacks = []; // 用于存放失败的回调
    let resolve = data => {
      if (data instanceof Promise3) {
        return data.then(resolve, reject); // 递归解析resolve中的参数，直到这个值是普通值
      }

      if (this.status === PENDING) {
        this.value = data;
        this.status = RESOLVED;
        this.onResolvedCallbacks.forEach(fn => fn());
      }
    };
    let reject = reason => {
      if (this.status === PENDING) {
        this.reason = reason;
        this.status = REJECTED;
        this.onRejectedCallbacks.forEach(fn => fn());
      }
    };
    try {
      fn(resolve, reject);
    } catch (e) {
      console.error(e, 'err-bai');
      reject(e);
    }
  }
  then(onFullfilled, onRejected) {
    // 当onFullfilled没有时，那么为了实现.then().then().then(data=>console.log(data))，可以正确返回，那么默认onFullfilled = v => {return v}
    onFullfilled = typeof onFullfilled === 'function' ? onFullfilled : v => v;
    onRejected =
      typeof onRejected === 'function'
        ? onRejected
        : err => {
            throw err;
          };
    let promise2 = new Promise3((resolve, reject) => {
      // 实现链式调用
      if (this.status === RESOLVED) {
        setTimeout(() => {
          try {
            let x = onFullfilled(this.value);
            // x可能是Promise
            console.log('onFullfilled');
            console.log('onFullfilled-promise2', promise2);
            resolvePromise(promise2, x, resolve, reject);
          } catch (err) {
            reject(err);
          }
        }, 0);
      }
      if (this.status === REJECTED) {
        setTimeout(() => {
          try {
            let x = onRejected(this.reason);
            console.log('onRejected');
            console.log('onRejected-promise2', promise2);
            resolvePromise(promise2, x, resolve, reject);
          } catch (err) {
            reject(err);
          }
        }, 0);
      }
      if (this.status === PENDING) {
        this.onResolvedCallbacks.push(() => {
          setTimeout(() => {
            try {
              // something to do……
              let x = onFullfilled(this.value);
              resolvePromise(promise2, x, resolve, reject);
            } catch (err) {
              reject(err);
            }
          }, 0);
        });
        this.onRejectedCallbacks.push(() => {
          setTimeout(() => {
            try {
              // something to do……
              let x = onRejected(this.reason);
              resolvePromise(promise2, x, resolve, reject);
            } catch (err) {
              reject(err);
            }
          }, 0);
        });
        console.log('等待状态……');
      }
    });
    return promise2;
  }
  // 支持.catch方法
  catch(errorCallback) {
    return this.then(null, errorCallback);
  }
  static resolve(data) {
    return new Promise3((resolve, reject) => {
      resolve(data);
    });
  }
  static reject(reason) {
    return new Promise3((resolve, reject) => {
      reject(reason);
    });
  }
}
Promise3.prototype.finally = function(callback) {
  return this.then(
    value => {
      return Promise3.resolve(callback()).then(() => value);
    },
    reason => {
      return Promise3.resolve(callback()).then(() => {
        throw reason;
      });
    },
  );
};

// Promise3.resolve(new Promise3((resolve, reject) => {
//   setTimeout(() => {
//     resolve('ok')
//   }, 1000)
// })).then(data => {
//   console.log(data)
// })
Promise3.resolve(456)
  .finally(() => {
    console.log('finally');
    return new Promise3((resolve, reject) => {
      setTimeout(() => {
        // resolve('ok')
        reject('fail');
      }, 2000);
    });
  })
  .then(data => {
    console.log(data, 'success');
  })
  .catch(err => {
    console.log(err, 'err');
  });
```
