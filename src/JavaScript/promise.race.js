const fs = require('fs').promises;

const isPromise = value => typeof value.then === 'function';

Promise.race = function(promises) {
  return new Promise((resolve, reject) => {
    // 遍历promises，拿到所有执行结果
    for (let i = 0; i < promises.length; i++) {
      let result = promises[i];
      if (isPromise(result)) {
        result.then(resolve, reject);
      } else {
        resolve(result);
      }
    }
  });
};

// 多个件接口采用快的那个，测试race方法
/**
Promise.race([
  fs.readFile('src/JavaScript/promisify.js', 'utf8'),
  fs.readFile('src/JavaScript/promise.js', 'utf8'),
  // 234,
  // '456',
])
  .then(data => {
    console.log('data===', data);
  })
  .catch(err => console.log(err, 'err==='));
 */

// 实现中断promise
let promise = new Promise((res, rej) => {
  setTimeout(() => {
    res('ok');
  }, 5 * 1000);
});

const wrap = promise => {
  let abort;
  let np = new Promise((res, rej) => {
    abort = rej;
  });
  let p = Promise.race([np, promise]);
  p.abort = abort;
  return p;
};

let p = wrap(promise);
p.then(data => {
  console.log(data);
}).catch(err => {
  console.log('err', err);
});
setTimeout(() => {
  p.abort('promise 超时');
}, 2 * 1000);

// 中断promise链

Promise.then(100)
  .then()
  .then(() => {
    // 在中间某一个then返回一个既不成功也不是失败的promise，就阻塞了。不能return别的
    return new Promise((res, rej) => {});
  })
  .then(
    data => {
      console.log(data);
    },
    err => {
      console.log(err);
    },
  );
