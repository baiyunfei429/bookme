---
nav:
  title: 【 JavaScript 】
  path: /components
---

## JS 手写

### 1、手写 js 代码

```tsx
import React from 'react';
import { JavaScript } from 'bjg-blog';

Array.prototype.myMap = function(callbackFn) {
  const arr = this;
  const resArr = new Array(arr.length);
  for (let i = 0; i < resArr.length; i++) {
    resArr[i] = callbackFn(arr[i], i, arr);
  }
  return resArr;
};

var res = [1, 2, 3].myMap((value, index, array) => {
  return value * 2;
});
// 返回 [2, 4, 6]

console.log('myMap', res);

export default () => <JavaScript title="Array.prototype.myMap" />;
```

```tsx
import React from 'react';
import { JavaScript } from 'bjg-blog';

Array.prototype.myReduce = function(callbackFn, init = 0) {
  const arr = this;
  let total = init;
  for (let i = 0; i < arr.length; i++) {
    total = callbackFn(total, arr[i], i, arr);
  }
  return total;
};

const initVal = 1;
var res = [1, 2, 3].myReduce((total, currentValue, index, arr) => {
  return total + currentValue;
}, initVal);
// 返回 9

console.log('myReduce', res);

export default () => <JavaScript title="Array.prototype.myReduce" />;
```

```tsx
import React from 'react';
import { JavaScript } from 'bjg-blog';

// functionA.apply(obj, [])
Function.prototype.myApply = function(context) {
  if (typeof this !== 'function') {
    throw new TypeError('Error');
  }
  context = context || window;
  context.fn = this;
  let result;
  console.log(arguments[1], 'arguments[1]');
  if (arguments[1]) {
    result = context.fn(...arguments[1]);
  } else {
    result = context.fn();
  }
  delete context.fn;
  return result;
};

var name = 'bai';
var obj = {
  name: 'yun',
};

function fn(a, b, c) {
  console.log(a + b + c + this.name);
}

fn.myApply(obj, ['我的', 'apply名字', '是']);

export default () => <JavaScript title="Array.prototype.myApply" />;
```

```tsx
import React from 'react';
import { JavaScript } from 'bjg-blog';

// functionA.call(obj, 1,2,3,4)
Function.prototype.myCall = function(context) {
  if (typeof this !== 'function') {
    throw new TypeError('Error');
  }
  context = context || window;
  context.fn = this;
  const args = [...arguments].slice(1);
  let result = context.fn(...args);
  delete context.fn;
  return result;
};

var name = 'bai1';
var obj = {
  name: 'yun1',
};

function fn(a, b, c) {
  console.log(a + b + c + this.name);
}

fn.myCall(obj, '我的', 'call名字', '是');

export default () => <JavaScript title="Array.prototype.myCall" />;
```

```tsx
import React from 'react';
import { JavaScript } from 'bjg-blog';

// functionA.bind(obj)
Function.prototype.myBind = function(context) {
  if (typeof this !== 'function') {
    throw new TypeError('Error');
  }
  const _this = this;
  const args = [...arguments].slice(1);
  return function F() {
    if (this instanceof F) {
      return new _this(...args, ...arguments);
    }
    return _this.apply(context, args.contact(...arguments));
  };
};

// let bai = {
//   name: 'bai'
// }

// let eat = function (food) {
//   this.food = food
//   console.log(`${this.name} eat ${this.food}`)
// }
// eat.prototype.sayFuncName = function () {
//   console.log('func name : eat')
// }

// let baiEat = eat.myBind(bai)
// let instance = new baiEat('orange') // bai eat orange

// console.log('instance:', instance) // {}

var name = 'bai1';
var obj = {
  name: 'yun1',
};

function fn(a, b, c) {
  console.log(a + b + c + this.name);
}

const f = fn.myBind(obj);
console.log(f);
f('我的', '名字bind', '是');

export default () => <JavaScript title="Array.prototype.myBind" />;
```
