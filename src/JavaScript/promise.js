const RESOLVED = 'RESOLVED';
const REJECTED = 'REJECTED';
const PENDING = 'PENDING';
// 所有的promise都要遵循规范，包括bluebird/q/es6-promise
const resolvePromise = (promise2, x, resolve, reject) => {
  // 循环引用，错误的实现
  if (promise2 === x) {
    return reject(
      new TypeError('Chaining cycle detected for promise #<Promise>'),
    );
  }
  let called; // 别人的promise可能既成功又失败，所以需要一个“锁”，保证只能出现一种结果
  if ((typeof x === 'object' && x != null) || typeof x === 'function') {
    // 有可能是promise，继续判断
    try {
      // 之所以需要try catch，是因为别人定义的promise有坑是Object.defineProperty(x, 'then', {get() {throw new Error();} })
      let then = x.then;
      if (typeof then === 'function') {
        // 只能是promise
        // 此处为什么不用call.then()来执行呢？因为get() {if(++index === 2) throw new Error()},也就是上面定义then时不报错，但是再次使用call.then()时，会再次取值，就报错了，别人的promise不完全可信
        then.call(
          x,
          y => {
            // 根据promise的状态决定是成功还是失败
            if (called) return;
            called = true;
            // resolve(y)
            // 如果y还是promise呢，需要递归处理
            resolvePromise(promise2, y, resolve, reject);
          },
          e => {
            if (called) return;
            called = true;
            reject(e);
          },
        );
      } else {
        // {then: '3333'}
        resolve(x);
      }
    } catch (err) {
      // 防止失败了，再次进入成功
      if (called) return;
      called = true;
      reject(err); // 取值出错直接返回reject
      console.error(err);
    }
  } else {
    // 如果不是object,不是function,也就是普通值，直接resolve
    resolve(x);
  }
  // console.log(promise2, x, resolve, reject)
};
class Promise {
  constructor(fn) {
    this.status = PENDING;
    this.value = undefined;
    this.reason = undefined;
    this.onResolvedCallbacks = []; // 用于存放成功的回调
    this.onRejectedCallbacks = []; // 用于存放失败的回调
    let resolve = data => {
      if (this.status === PENDING) {
        this.value = data;
        this.status = RESOLVED;
        this.onResolvedCallbacks.forEach(fn => fn());
      }
    };
    let reject = reason => {
      if (this.status === PENDING) {
        this.reason = reason;
        this.status = REJECTED;
        this.onRejectedCallbacks.forEach(fn => fn());
      }
    };
    try {
      fn(resolve, reject);
    } catch (e) {
      console.error(e, 'err-bai');
      reject(e);
    }
  }
  then(onFullfilled, onRejected) {
    // 当onFullfilled没有时，那么为了实现.then().then().then(data=>console.log(data))，可以正确返回，那么默认onFullfilled = v => {return v}
    onFullfilled = typeof onFullfilled === 'function' ? onFullfilled : v => v;
    onRejected =
      typeof onRejected === 'function'
        ? onRejected
        : err => {
            throw err;
          };
    let promise2 = new Promise((resolve, reject) => {
      // 实现链式调用
      if (this.status === RESOLVED) {
        setTimeout(() => {
          try {
            let x = onFullfilled(this.value);
            // x可能是Promise
            console.log('onFullfilled');
            console.log('onFullfilled-promise2', promise2);
            resolvePromise(promise2, x, resolve, reject);
          } catch (err) {
            reject(err);
          }
        }, 0);
      }
      if (this.status === REJECTED) {
        setTimeout(() => {
          try {
            let x = onRejected(this.reason);
            console.log('onRejected');
            console.log('onRejected-promise2', promise2);
            resolvePromise(promise2, x, resolve, reject);
          } catch (err) {
            reject(err);
          }
        }, 0);
      }
      if (this.status === PENDING) {
        this.onResolvedCallbacks.push(() => {
          setTimeout(() => {
            try {
              // something to do……
              let x = onFullfilled(this.value);
              resolvePromise(promise2, x, resolve, reject);
            } catch (err) {
              reject(err);
            }
          }, 0);
        });
        this.onRejectedCallbacks.push(() => {
          setTimeout(() => {
            try {
              // something to do……
              let x = onRejected(this.reason);
              resolvePromise(promise2, x, resolve, reject);
            } catch (err) {
              reject(err);
            }
          }, 0);
        });
        console.log('等待状态……');
      }
    });
    return promise2;
  }
}

Promise.defer = Promise.deferred = function() {
  let dfd = {};
  dfd.promise = new Promise((resolve, reject) => {
    dfd.resolve = resolve;
    dfd.reject = reject;
  });
  return dfd;
};

module.exports = Promise;
