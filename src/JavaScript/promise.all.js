const fs = require('fs').promises;

const isPromise = value => typeof value.then === 'function';

Promise.all = function(promises) {
  return new Promise((resolve, reject) => {
    // 遍历promises，拿到所有执行结果
    const arr = [];
    let idx = 0;
    const processData = (index, data) => {
      arr[index] = data; // 不能使用数组长度来判断，因为可能会出现index靠后的结果先返回，比如[].3 = result，那么数组的长度会变成3，只是前两个是empty：[ <2 empty items>, 1 ]
      // 使用数组长度判断是不对的
      // if (arr.length == promises.length) {
      //   resolve(arr)
      // }
      if (++idx === promises.length) {
        resolve(arr);
      }
    };
    for (let i = 0; i < promises.length; i++) {
      let result = promises[i];
      if (isPromise(result)) {
        result.then(data => {
          processData(i, data);
          //
        }, reject);
      } else {
        processData(i, result);
      }
    }
  });
};

// promise 缺陷默认是无法中断的，只是不采用返回的结果，包括fetch的api也是如此
Promise.all([
  fs.readFile('src/JavaScript/promise.js', 'utf8'),
  fs.readFile('src/JavaScript/promisify.js', 'utf8'),
  234,
  '456',
])
  .then(data => {
    console.log('data===', data);
  })
  .catch(err => console.log(err, 'err==='));
