---
nav:
  title: 【 JavaScript 】
  path: /components
---

## font

事情要从一个 jira 说起。  
jira 内容不方便具体说，一句描述：页面显示的繁体字。有个帳字展示出来比相邻的几个字细一圈。

我的第一反应，是不是这个字有 bug 吧。
然后去右边 style 里修改为 700，突然就显示一致了。我以为此 jira 不过如此。

后面内容：
更改@ font-face 源
unicode-range 定义特殊字符
JavaScript 中 FontFace 对象
粗略解决方案，pc 和移动端分开定义样式
font-weight 的权重
