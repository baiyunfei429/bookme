---
nav:
  title: 【 JavaScript 】
  path: /components
---

## Typescript

### interface 和 type

用 interface 描述**数据结构**，用 type 描述**类型关系**  
`相同点`：  
1)、都可以描述一个对象或者函数，注意：语法不同

**interface**

```javascript
interface User {
  name: string
  age: number
}

interface SetUser {
  (name: string, age: number): void;
}
```

**type**

```javascript
type User = {
  name: string
  age: number
};

type SetUser = (name: string, age: number)=> void;
```

2)、都允许拓展（extends）
interface 和 type 都可以拓展，并且两者并不是相互独立的，也就是说 interface 可以 extends type, type 也可以 extends interface 。 虽然效果差不多，但是两者语法不同。

**type extends type**

```javascript
type Name = {
  name: string,
};
type User = Name & { age: number };
```

**interface extends interface**

```javascript
interface Name {
  name: string;
}
interface User extends Name {
  age: number;
}
```

`不同点`：  
1)、type 可以而 interface 不行：type 可以声明基本类型别名，联合类型，元组等类型

```javascript
// 基本类型别名
type Name = string

// 联合类型
interface Dog {
    wong();
}
interface Cat {
    miao();
}

type Pet = Dog | Cat

// 具体定义数组每个位置的类型
type PetList = [Dog, Pet]
```

2)、type 可以而 interface 不行：type 语句中还可以使用 typeof 获取实例的 类型进行赋值 3)、interface 可以而 type 不行：interface 能够声明合并。interface 可以被 implements

```javascript
interface User {
  name: string
  age: number
}

interface User {
  sex: string
}

/*
User 接口为 {
  name: string
  age: number
  sex: string
}
*/
```

不准确来说，如果不清楚什么时候用 interface/type，能用 interface 实现，就用 interface , 如果不能就用 type。

interface 是接口，type 是类型，本身就是两个概念。只是碰巧表现上比较相似。
希望定义一个变量类型，就用 type，如果希望是能够继承并约束的，就用 interface。
如果你不知道该用哪个，说明你只是想定义一个类型而非接口，所以应该用 type。
两者都是为了告诉编译器，如何理解某个字段的结构类型

但两者的定义和使用场景还是有区别的：

interface 是一种关系结构的描述，里面可以包含属性和方法，可派生
type 是一种表达式，所以也可以说是一种 aliase，可以使用一些表达式的操作符，并且通过这些操作符实现和 interface 近似等价的关系描述
所以，在描述带关系的数据结构时，interface 应该优先于 type 被考虑，甚至可以简化思考，直接上 interface。而 type 在一定程度上简化类型描述，例如，type StrOrNum = string | number，后面都可以复用 StrOrNum 去代表 string | number，如果在一个类型描述文件里，string | number 这样的类型字段比较多，就可以用 type 去精简内容。

### 参考文章列表

---

react 官网： https://juejin.cn/post/6844903749501059085
