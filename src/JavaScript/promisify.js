const fs = require('fs');
const util = require('util');

// 实现util模块下的promisify方法
const promisify = fn => (...args) =>
  new Promise((resolve, reject) => {
    fn(...args, (err, data) => {
      if (err) reject(err);
      resolve(data);
    });
  });

let read = promisify(fs.readFile);
// let read = util.promisify(fs.readFile)

read('src/JavaScript/promise.js', 'utf8').then(data => {
  console.log(data);
});
