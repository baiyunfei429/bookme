import React from 'react';

export default ({ title }: { title: string }) => <strong>{title}</strong>;
