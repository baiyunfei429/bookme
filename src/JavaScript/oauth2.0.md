---
nav:
  title: 【 JavaScript 】
  path: /components
---

## oauth2.0

概念：
1、国际通用的 OAuth2.0 协议
2、

2.2 接入方式
QQ 登录使用国际通用的 OAuth2.0 协议进行验证与授权，网站可通过两种方式接入：
（1）使用 QQ 互联提供的 SDK 包，用户体验统一，只需要修改少量代码，不需要理解验证授权流程，需要快速接入 QQ 登录的应用可选用此方法。
详见：【QQ 登录】SDK 下载
QQ 登录 JS SDK 详见：【QQ 登录】JS SDK 使用说明
（2）根据 QQ 登录 OAuth2.0 协议，自主开发，此方法自定义程度较高，需要与现有系统进行整合的网站可选用此方法。
详见：【QQ 登录】OAuth2.0 开发文档
