---
nav:
  title: 知识点
  path: /components
---

## 知识点

此处标题纯粹是为了把右上角的命名更改，别无他用  
因为此版本的 dumi，按照字典序最后一个模块的 nav-title 即是右上角显示的模块名称
