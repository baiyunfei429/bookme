---
nav:
  title: 【 前端测试 】
  path: /components
---

## Test

Demo:

```tsx
import React from 'react';
import { Test } from 'bjg-blog';

export default () => <Test title="前端测试" />;
```
