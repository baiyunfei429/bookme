---
nav:
  title: 【 Vue基础 】
  path: /components
---

## Vue3 新属性

vue3+vite

Demo:

```tsx
import React from 'react';
import { Vue } from 'bjg-blog';

export default () => <Vue title="First vue" />;
```
