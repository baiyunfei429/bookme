---
nav:
  title: 【 Node 】
  path: /components
---

## Node

### 版本号规则

npm 中版本的格式

major.minor.patch  
主版本号.次版本号.修补版本号

~version  
大概匹配某个版本  
~1.2.3，表示 >= 1.2.3 并且 <1.3.0 的中间版本

^version  
兼容某个版本  
^1.2.3，表示 >= 1.2.3 并且 <2.0.0  
^0.2.3，表示 >= 0.2.3 并且 <0.3.0

### 升级依赖包

如何升级 package.json 中的依赖包？  
执行下面命令查看仓库中一些过时的依赖列表

```
npm outdated
```

![执行npm outdated](../assets/images/node-update-npm-1.png)

通常按照的依赖版本是：^1.2.3  
如果有新的次版本(1.2->1.3)或补丁版本(1.2.3->1.2.4)，执行

```
npm update
```

则已安装的版本会被更新，（会更新模糊版本至最新），并且 package-lock.json 文件会被新版本填充。但是，有些是主版本，运行 npm update 不会更新那些主版本，因为他们的更新会引入巨大改变，所以 npm 希望为你减少麻烦。  
![执行npm outdated](../assets/images/node-update-npm-2.png)  
![执行npm outdated](../assets/images/node-update-npm-3.png)  
如果要更新主版本，那么，全局安装 npm-check-updates 软件包，执行

```
npm install -g npm-check-updates
ncu -u
```

这会升级 package.json 文件的 dependencies 和 devDependencies 中的所有版本，以便 npm 可以安装新的主版本。
然后运行 npm update。  
依赖包需重新安装，执行 npm install
