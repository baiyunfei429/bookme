---
nav:
  title: 【 Node 】
  path: /components
---

## NPM

npm config get registry // 查看 npm 当前镜像源

npm config set registry https://registry.npm.taobao.org/ // 设置 npm 镜像源为淘宝镜像

yarn config get registry // 查看 yarn 当前镜像源

yarn config set registry https://registry.npm.taobao.org/ // 设置 yarn 镜像源为淘宝镜像

npm --- https://registry.npmjs.org/

cnpm --- https://r.cnpmjs.org/

taobao --- https://registry.npm.taobao.org/
