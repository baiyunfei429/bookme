---
hero:
  title: bjg' blog
  desc: bjg' steps of jg
  actions:
    - text: Getting Started
      link: /components
features:
  - icon: https://gw.alipayobjects.com/zos/bmw-prod/881dc458-f20b-407b-947a-95104b5ec82b/k79dm8ih_w144_h144.png
    title: Feature 1
    desc: Balabala
  - icon: https://gw.alipayobjects.com/zos/bmw-prod/d60657df-0822-4631-9d7c-e7a869c2f21c/k79dmz3q_w126_h126.png
    title: Feature 2
    desc: Balabala
  - icon: https://gw.alipayobjects.com/zos/bmw-prod/d1ee0c6f-5aed-4a45-a507-339a4bfe076c/k7bjsocq_w144_h144.png
    title: Feature 3
    desc: Balabala
footer: Copyright © baiyunfei429 | [京公网安备11011402012128号](http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=11011402012128) | [京ICP备2021036171号](https://beian.miit.gov.cn)
---

## bjg' blog!
